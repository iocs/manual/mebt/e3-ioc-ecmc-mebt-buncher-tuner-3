epicsEnvSet("IOC", "MEBT-010:Ctrl-ECAT-003")

require essioc
require ecmccfg 8.0.0

iocshLoad ${ecmccfg_DIR}startup.cmd "ECMC_VER=8.0.2, NAMING=ESSnaming"

iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EK1100"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL1018"
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL2808"

# Set all outputs to feed switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput03,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput04,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput07,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput08,1)"

iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL5101"
iocshLoad ${E3_CMD_TOP}/iocsh/configureSlaveLocal.cmd, "HW_DESC=EL7047, CONFIG=-Motor-Mclennan-HT18C230"

#- Max full step freq = 8000Hz (setting is 3)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x5,3,1)"

## AXIS 1
iocshLoad ($(ecmccfg_DIR)configureAxis.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/ecmcMCU-ESSB-MobileTuner_Axis1.ax, DEV=MEBT-010:MC-MCU-003")

# Tuner Feedback Loop
dbLoadRecords "$(E3_CMD_TOP)/tunerFeedbackLoop.db" "P=MEBT-010:MC-MCU-003:, CavField=MEBT-010:RFS-Cav-310:CavFld-Wave-High, OpenLoop=MEBT-010:RFS-DIG-301:OpenLoop-RB, FreqDelta=MEBT-010:RFS-LLRF-301:FFFreqTrackDeltaF"

iocshLoad "${essioc_DIR}/common_config.iocsh"

ecmcConfigOrDie "Cfg.EcApplyConfig(1)"
iocshLoad ($(ecmccfg_DIR)setAppMode.cmd)
